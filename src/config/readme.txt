ProWorks Umbraco v7 Media Image Lightbox

This package is a simple UI enhancement to the v7 media image upload thumbnails to open the image in a lightbox instead of a new browser tab.

Git repo: https://bitbucket.org/proworks/proworks-umbraco-media-lightbox
