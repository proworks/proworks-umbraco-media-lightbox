# ProWorks v7 Media Image Lightbox

This package is a simple UI enhancement to the v7 media image upload thumbnails to open the image in a lightbox instead of a new browser tab.

## Setup for Development

### Install Dependencies

```bash
cd src
npm install -g grunt-cli
npm install
```

### Build

```bash
grunt
```

### Watch

```bash
grunt watch
```

### Package

```bash
grunt package
```
