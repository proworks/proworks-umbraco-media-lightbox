﻿//bootstrap controller onto page
$(document).ready(function () {
	var bodyEl = $('body');
	var lightbox = lity();

	bodyEl.on('DOMNodeInserted', 'li.thumbnail a', function () {
	    var $this = $(this); 
	    if ($this.length > 0) { 
			if(!$this.is(".pw-lightbox"))
			{
				//lightbox($this.attr("href"));
				$this.on('click', function(){
					var $this = $(this);
					lightbox.open($this.attr("href"));
					return false;
				});
				$this.addClass("pw-lightbox");
			}
	    }
	});

});